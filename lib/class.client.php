<?php
/**
 * Project-A-Client
 *  
 * PHP Version 5
 *
 * @category CLI_App
 * @package  ProjectACLient
 * @author   Joël Morren <jo.morren@gmail.com>
 * @license  (c) 2015 
 * @link     project-a
 */
    
/** Client class
 *
 * @category CLI_App
 * @package  ProjectAClient
 * @author   Joël Morren <jo.morren@gmail.com>
 * @license  (c) 2015
 * @link     project-a
 */

class Client {

	public $images = array(
		'images' => array()
	);
	public $url     = 'http://192.168.2.249';
	public $project = 'raspart';

	/**
	 * Main
	 *
	 * @return void
	 */
	public function main() {
		$command = '/usr/bin/fbi -a -t 30 -T 2  -readahead -noverbose /home/pi/images/*';
		if ($this->isRunning()) {
			$this->getImages();
			shell_exec('killall fbi'); //Zou deze vervangen dat hij alleen het vorig pid afsluit die op geslagen is bij de functie $this->running
			shell_exec($command);
		}
	}

	/**
	 * Main
	 *
	 * @return void
	 */
	public function getImages() {
		$this->proccesJSON();
		$this->processArray();
	}

	/**
	 * Main
	 *
	 * @return void
	 */
	public function getJSON() {
		$json = $this->useCurl($this->url . '/' . $this->project);
		
		return $json;
	}

	/**
	 * Procces JSON
	 *
	 * @return array with images
	 */
	public function proccesJSON() {
		$content = json_decode($this->getJSON(), true);
		foreach ($content['images'] as $name) {
			array_push($this->images['images'], $name);
		}
	}

	/**
	 * use curl
	 *
	 * @return url content
	 */
	public function useCurl() {
		$timeout = 5;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

		$rv = curl_exec($ch);

		curl_close($ch);

		return $rv;
	}

	/**
	 * Download images form server
	 *
	 * @return void
	 */
	public function processArray() {
		foreach ($this->images['images'] as $image) {
			$this->downloadImages($image);
		}
	}

	/**
	 * Save images to file
	 *
	 * @param string $image name & size of images to download
	 * @return void
	 */
	public function downloadImages($image) {
 //Ik zou deze functie samenvoegen met $this->saveImages()
		$url = $this->url . '/' . $this->project .'/' . $image['name'];

		$this->saveImages($url, $image);
	}

	/**
	 * save images to file
	 *
	 * @param string $url   image file
	 * @param string $image name of file
	 * @return void
	 */
	public function saveimages ($url, $image) {
 //Ik zou deze functie samenvoegen met $this->downloadImages()
		$filelocation = '/home/pi/images/' . $image['name'];
		file_put_contents($filelocation, file_get_contents($url));
	}

	/**
	 * checks if script is running
	 *
	 * @return true/false
	 */
//Deze functie zou ik herschrijven dat hij controleert of het oude pid nog bestaat. als dat zo is geeft de functie een true terug, mocht het pid niet bekend zijn slaat hij zijn eigen pid op en daarna pas het script verder starten
	public function isRunning () {
		$file = __DIR__.'../status/run/1';

		if (file_exists($file) {
			return true;
		} else {
			return false;
		}
	}

}
