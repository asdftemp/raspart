#!/usr/bin/env php
<?php
/**
 * Sense Module DeltaUpdater
 *
 * PHP Version 5
 *
 * @category Executables
 * @package  ProjectAClient
 * @author   Joël Morren <jo.morren@gmail.com>
 * @license  (c) 2015 Gert & Joël
 * @link     http://project-a.nl
 */
require_once __DIR__.'/../lib/class.client.php';

$PC = new Client();
$PC->main();
